# Rakuten DevOps Challenge Solution

## Usage

To start and provision the VM:

```sh
vagrant up
```

Once started, access the app via:

```sh
curl 10.10.10.20
```

## Considerations

- Using `ansible_local` for provisioning, so Ansible doesn't need to be present on host
- App code is pulled from Git, only works for public repos
- Python virtualenv to avoid conflicts with other packages
- [Gunicorn][gunicorn] as WSGI server controlled by [Supervisor][supervisor]
- Number of Gunicorn workers depending on number of CPUs of VM
- Logrotate to rotate logs for simplicy - drawbacks, see [logrotate.conf][lgrt-conf]

## Possible Improvements

- Refactor code to use Ansible modules for better structure
- Use iptables to block traffic to wsgi port (though it's bound only to 127.0.0.1 already)

[gunicorn]: http://gunicorn.org/
[supervisor]: http://supervisord.org/
[lgrt-conf]: ./ansible/templates/logrotate.conf
